import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StoreComponent from "./store.component";
import BoardElement from "../domains/board-element.domain";

import { FaStar } from 'react-icons/fa'

class BoardElementComponent extends Component {

  getInfo = () => {

    const { element, translate } = this.props;

    return `${element.getAge()} - ${translate.GENDERS[ element.gender ]} - ${element.location}`;
  };

  render() {

    const { element } = this.props;

    return (
      <div className={`board-element`}>

        <img className={`board-element__image`} src={element.image} alt={element.name} />

        <div className={`board-element__name`}>
          {element.name}
        </div>

        <div className={`board-element__score`}>
          <FaStar />
          {element.score}
        </div>

        <div className={`board-element__info`}>
          {this.getInfo()}
        </div>

      </div>
    );
  }

}

BoardElementComponent.propTypes = {
  element: PropTypes.instanceOf(BoardElement)
};

export default StoreComponent(BoardElementComponent);