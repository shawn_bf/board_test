import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Board from "../domains/board.domain";
import BoardElementComponent from '../components/board-element.component';
import DroppableComponent from "./droppable.component";
import DraggableComponent from "./draggable.component";
import DragDropInfo from "../domains/drag-drop-info.domain";
import { FaEllipsisH } from 'react-icons/fa';
import { DROP_LIST } from "../enums/droplist.enum";
import TooltipComponent from "./tooltip.component";
import StoreComponent from "./store.component";

class BoardComponent extends Component {

  constructor( props ) {
    super(props);

    this.state = {
      showTooltip: false
    }

  }

  toggleTooltip = () => {

    this.setState(( prevState ) => ({
      showTooltip: !prevState.showTooltip
    }));

  };


  render() {

    const { board, translate } = this.props;
    const { showTooltip } = this.state;
    const tooltipItems = [
      {
        text: translate.BOARD.TOOLTIP.REMOVE_BOARD,
        clicked: () => this.props.removeBoard(board)
      }
    ];

    return (
      <div className={`board`}>

        <DroppableComponent dropInfo={new DragDropInfo({ dropList: DROP_LIST.BOARD, itemId: board.id })}>

          <div className={`board__header`}>

            <div className={`board__name`}>
              {board.name}
            </div>

            <div className={`board__amount`}>
              ({board.elements.length})
            </div>

            <div className={`board__tooltip`}>
              <span className={`board__tooltip-icon`} onClick={this.toggleTooltip}>
                <FaEllipsisH />
              </span>
              {
                showTooltip && <TooltipComponent items={tooltipItems} />
              }
            </div>

          </div>

          <div className={`board__elements`}>

            {
              board.elements.map(( element, index ) => (
                <DraggableComponent key={element.id}
                                    dragInfo={new DragDropInfo({
                                      dropList: DROP_LIST.BOARD,
                                      itemId: element.id,
                                      index
                                    })}>
                  <BoardElementComponent element={element} />
                </DraggableComponent>
              ))
            }

          </div>

        </DroppableComponent>

      </div>
    );
  }

}

BoardComponent.propTypes = {
  board: PropTypes.instanceOf(Board)
};

export default StoreComponent(BoardComponent);