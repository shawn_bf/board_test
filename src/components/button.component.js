import React, { Component } from 'react';
import PropTypes from 'prop-types';


class ButtonComponent extends Component {

  render() {

    const { className, disabled, text, onClick } = this.props;

    return (
      <button className={`button ${className} ${disabled ? 'disabled' : ''}`}
              onClick={onClick}>
        {text}
      </button>
    );
  }

}

ButtonComponent.propTypes = {
  text: PropTypes.any.isRequired,
  className: PropTypes.any,
  onClick: PropTypes.func,
  disabled: PropTypes.bool
};

export default ButtonComponent;