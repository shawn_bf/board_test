import React, { Component } from 'react';
import PropTypes from "prop-types";
import DragDropInfo from "../domains/drag-drop-info.domain";
import StoreComponent from "./store.component";

class DraggableComponent extends Component {

  dragStart = () => {
    const { dragStarted, dragInfo } = this.props;

    dragStarted(dragInfo);
  };

  dragEnded = () => {
    const { dragEnded, dragInfo } = this.props;

    dragEnded(dragInfo);
  };

  render() {

    const { children } = this.props;

    return (
      <div className={`draggable`}
           draggable
           onDragStart={this.dragStart}
           onDragEnd={this.dragEnded}>
        {children}
      </div>
    );
  }

}

DraggableComponent.propTypes = {
  dragInfo: PropTypes.instanceOf(DragDropInfo).isRequired
};

export default StoreComponent(DraggableComponent);