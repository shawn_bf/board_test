import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StoreComponent from "./store.component";
import DragDropInfo from "../domains/drag-drop-info.domain";

class DroppableComponent extends Component {

  onDrop = ( event ) => {

    const { dropped, dropInfo } = this.props;
    const classNameDraggable = 'draggable';

    let { target } = event;
    let findIdx = -1;
    if ( target ) {
      do {
        target = target.parentNode;
      } while ( !target.classList.contains(classNameDraggable) );

      target.parentNode.childNodes.forEach(( node, index ) => {
        if ( node === target ) {
          findIdx = index;
        }
      });

    }

    dropped(new DragDropInfo({ ...dropInfo, index: findIdx }));
  };

  render() {

    const { children } = this.props;

    return (
      <div className={`droppable`}
           onDrop={( event ) => {
             this.onDrop(event)
           }}
           onDragOver={( event ) => event.preventDefault()}>
        {children}
      </div>
    );
  }

}

DroppableComponent.propTypes = {
  dropInfo: PropTypes.instanceOf(DragDropInfo)
};

export default StoreComponent(DroppableComponent);