import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { INPUT_TYPES } from "../enums/input-types.enum";

const ENTER_CODE = 13;

class InputComponent extends Component {

  componentWillMount() {
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    this.focusInput();
  }


  onChange = ( e ) => {
    this.props.onChange(e.target.value);
  };

  onKeyDown = ( e ) => {

    if ( e.keyCode === ENTER_CODE ) {
      this.props.onEnter();
    }

  };

  focusInput = () => {
    if ( this.inputRef ) {
      this.inputRef.current.focus();
    }
  };

  render() {

    const { className, focus, placeholder, type, value } = this.props;

    return (
      <input ref={this.inputRef}
             className={`input ${className}`}
             placeholder={placeholder}
             type={type}
             value={value}
             onChange={this.onChange}
             onKeyUp={this.onChange}
             onKeyDown={this.onKeyDown}
      />
    );
  }

}

InputComponent.propTypes = {
  className: PropTypes.any,
  focus: PropTypes.bool,
  placeholder: PropTypes.string,
  type: PropTypes.oneOf(Object.keys(INPUT_TYPES).map(inputType => INPUT_TYPES[ inputType ])),
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  onEnter: PropTypes.func
};

InputComponent.defaultProps = {
  type: INPUT_TYPES.TEXT
};

export default InputComponent;