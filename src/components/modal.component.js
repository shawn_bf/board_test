import React, { Component } from 'react';
import StoreComponent from "./store.component";
import PropTypes from 'prop-types';

const ESC_KEYCODE = 27;

class ModalComponent extends Component {

  closeModal = () => {
    this.props.closeModal();
  };

  onKeyDown = ( e ) => {

    if ( e.keyCode === ESC_KEYCODE ) {
      this.closeModal();
    }

  };

  render() {

    const { header, content, footer, className } = this.props;

    return (
      <div className={`modal ${className}`}
           onClick={this.closeModal}
           onKeyDown={this.onKeyDown}>

        <div className={`modal__container`} onClick={( e ) => e.stopPropagation()}>

          <div className={`modal__header`}>
            {header}
          </div>

          <div className={`modal__content`}>
            {content}
          </div>

          <div className={`modal__footer`}>
            {footer}
          </div>

        </div>

      </div>
    );
  }

}

ModalComponent.propTypes = {

  header: PropTypes.any,
  content: PropTypes.any,
  footer: PropTypes.any,
  className: PropTypes.any

};

export default StoreComponent(ModalComponent);