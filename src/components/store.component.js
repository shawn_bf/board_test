import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { DRAG_ENDED, DRAG_STARTED, DROPPED } from "../store/drag-drop/drag-drop.actions";
import { ADD_NEW_BOARD, MOVE_ITEM_TO_LIST, REMOVE_BOARD } from "../store/board/board.actions";
import { CLOSE_MODAL, OPEN_MODAL } from "../store/modal/modals.actions";

const StoreComponent = ( WrappedComponent ) => {
  class StoreComponentHOC extends Component {
    render() {
      return (
        <WrappedComponent {...this.props} />
      );
    }
  }

  return connect(mapStateToProps, mapDispatchToProps)(StoreComponentHOC);

};

const mapStateToProps = ( state ) => ({
  ...state
});

const mapDispatchToProps = ( dispatch ) =>
  bindActionCreators(
    {
      dragStarted: payload => DRAG_STARTED(payload),
      dragEnded: () => DRAG_ENDED(),
      dropped: payload => DROPPED(payload),
      moveItemToOtherList: payload => MOVE_ITEM_TO_LIST(payload),
      openModal: payload => OPEN_MODAL(payload),
      closeModal: payload => CLOSE_MODAL(payload),
      addNewBoard: payload => ADD_NEW_BOARD(payload),
      removeBoard: payload => REMOVE_BOARD(payload),
    },
    dispatch
  );

export default StoreComponent;