import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TooltipComponent extends Component {

  render() {

    const { items } = this.props;

    return (
      <div className={`tooltip`}>

        {
          items.map(( item ) => (

            <div className={`tooltip__item`} onClick={item.clicked}>
              {item.text}
            </div>

          ))
        }

      </div>
    );
  }

}

TooltipComponent.propTypes = {

  items: PropTypes.array.isRequired

};

export default TooltipComponent;