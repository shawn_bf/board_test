import UUID from 'uuid';
import moment from "moment";
import { DATE_FORMATS } from "../enums/date-formats.enum";

class BoardElement {

  constructor( { id, name, birthDay, score, image, gender, location } ) {
    this.id = id || UUID();
    this.name = name;
    this.birthDay = moment(birthDay, DATE_FORMATS.NORMAL);
    this.score = score;
    this.image = image;
    this.gender = gender;
    this.location = location;

  }

  getAge = () => {
    return moment().diff(moment(this.birthDay), 'years');
  }

}

export default BoardElement;