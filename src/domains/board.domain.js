import UUID from 'uuid';

class Board {

  constructor( { id, name, elements } ) {
    this.id = id || UUID();
    this.name = name;
    this.elements = elements || [];

  }

}

export default Board;