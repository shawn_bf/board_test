class DragDropInfo {

  constructor( { dropList, itemId, index } ) {
    this.dropList = dropList;
    this.itemId = itemId;
    this.index = index;
  }

}

export default DragDropInfo;