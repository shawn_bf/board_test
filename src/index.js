import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import './style/style.scss';
import store from './store';
import AppRouter from './router';
import { Provider } from "react-redux";
import './support-drag-drop-mobile';
import Modals from './modals';

const App = () => (
  <Provider store={store}>
    <Fragment>
      <AppRouter />
      <Modals />
    </Fragment>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
