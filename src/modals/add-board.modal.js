import React, { Component } from 'react';
import StoreComponent from "../components/store.component";
import ModalComponent from "../components/modal.component";
import InputComponent from "../components/input.component";
import { INPUT_TYPES } from "../enums/input-types.enum";
import ButtonComponent from "../components/button.component";

class AddBoardModal extends Component {

  constructor( props ) {
    super(props);

    this.state = {
      boardName: ''
    }

  }

  changeBoardName = ( newBoardName ) => {
    this.setState({ boardName: newBoardName });
  };

  addNewBoard = () => {
    if ( this.state.boardName ) {
      this.props.addNewBoard(this.state.boardName);
      this.closeModal();
    }
  };

  closeModal = () => {
    this.props.closeModal();
  };

  render() {

    const { translate } = this.props;
    const { boardName } = this.state;

    const header = <h1 className={`add-board-modal__header`}>{translate.MODALS.ADD_NEW_BOARD.TITLE}</h1>;

    const content = <div className={`add-board-modal__content form`}>

      <div className={`form__control`}>

        <span className={`form__label`}>
          {translate.MODALS.ADD_NEW_BOARD.LABEL}
        </span>

        <InputComponent className={`add-board-modal__new-board-name form__input`}
                        focus={true}
                        placeholder={translate.MODALS.ADD_NEW_BOARD.INPUT_PLACEHOLDER}
                        type={INPUT_TYPES.TEXT}
                        value={boardName}
                        onChange={this.changeBoardName}
                        onEnter={this.addNewBoard} />

      </div>

    </div>;

    const footer = <div className={`add-board-modal__footer`}>

      <div className={`button__container`}>

        <ButtonComponent text={translate.MODALS.ADD_NEW_BOARD.BUTTONS.CANCEL}
                         className={`button__cancel`}
                         onClick={this.closeModal} />

        <ButtonComponent text={translate.MODALS.ADD_NEW_BOARD.BUTTONS.ADD}
                         className={`button__submit`}
                         disabled={!boardName.length}
                         onClick={this.addNewBoard} />
      </div>

    </div>;

    return (

      <ModalComponent className={`add-board-modal`}
                      header={header}
                      content={content}
                      footer={footer} />

    );
  }

}

export default StoreComponent(AddBoardModal);