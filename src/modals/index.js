import React, { Component, Fragment } from 'react';
import StoreComponent from "../components/store.component";
import AddBoardModal from "./add-board.modal";
import { MODALS } from "../enums/modals.enum";

class Modals extends Component {

  closeModal = () => {
    this.props.closeModal();
  };

  getCorrectModal = () => {

    switch ( this.props.modal ) {

      case MODALS.ADD_NEW_BOARD: {

        return <AddBoardModal />
      }

      default: {
      }

    }
  };

  render() {
    return (
      <Fragment>
        {this.getCorrectModal()}
      </Fragment>
    );
  }

}

export default StoreComponent(Modals);