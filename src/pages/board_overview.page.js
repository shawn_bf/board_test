import React, { Component } from 'react';
import StoreComponent from "../components/store.component";
import BoardComponent from '../components/board.component';
import DroppableComponent from "../components/droppable.component";
import DragDropInfo from "../domains/drag-drop-info.domain";
import DraggableComponent from "../components/draggable.component";
import { DROP_LIST } from "../enums/droplist.enum";

import { FaPlusCircle } from 'react-icons/fa'
import { MODALS } from "../enums/modals.enum";

class BoardOverviewPage extends Component {

  componentDidUpdate = ( prevProps ) => {

    const { dragDrop, moveItemToOtherList } = prevProps;

    if ( dragDrop && dragDrop.dragStarted && dragDrop.dropped ) {
      moveItemToOtherList(dragDrop);
    }

  };

  onAddNewBoard = () => {
    this.props.openModal(MODALS.ADD_NEW_BOARD);
  };

  render() {

    const { boards } = this.props;

    return (
      <div className={`page board-overview`}>

        <div className={`page__content`}>
          <DroppableComponent dropInfo={new DragDropInfo({ dropList: DROP_LIST.BOARD_OVERVIEW })}>

            {
              boards.map(( board, index ) => (
                <DraggableComponent key={board.id}
                                    dragInfo={new DragDropInfo({
                                      dropList: DROP_LIST.BOARD_OVERVIEW,
                                      itemId: board.id,
                                      index
                                    })}>
                  <BoardComponent board={board} />
                </DraggableComponent>
              ))
            }

          </DroppableComponent>
        </div>

        <div className={`page__footer board-overview__add-new-board`}>

          <span onClick={this.onAddNewBoard}>
            <FaPlusCircle />
          </span>

        </div>

      </div>
    );
  }
}

export default StoreComponent(BoardOverviewPage);