import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { ROUTES } from "./routes";
import { ROUTE_PATHS } from "../enums/route-paths.enum";

class AppRouter extends Component {

  render() {
    return (
      <Router>

        <Switch>

          {
            ROUTES.map(route => (
              <Route key={route.path} exact path={route.path} component={route.component} />
            ))
          }

          <Redirect to={ROUTE_PATHS.DEFAULT} />


        </Switch>

      </Router>
    )
  }

}

export default AppRouter;