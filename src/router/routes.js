import Route from "../domains/route.domain";
import { ROUTE_PATHS } from "../enums/route-paths.enum";
import BoardOverviewPage from "../pages/board_overview.page";

export const ROUTES = [

  new Route({ path: ROUTE_PATHS.BOARD_OVERVIEW, component: BoardOverviewPage })

];