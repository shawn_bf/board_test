import BOARD_ACTION_TYPES from "./board.action-types";

export const MOVE_ITEM_TO_LIST = payload => dispatch => {

  dispatch({
    type: BOARD_ACTION_TYPES.MOVE_ITEM_TO_LIST,
    payload
  });

};

export const ADD_NEW_BOARD = payload => dispatch => {

  dispatch({
    type: BOARD_ACTION_TYPES.ADD_NEW_BOARD,
    payload
  });

};

export const REMOVE_BOARD = payload => dispatch => {

  dispatch({
    type: BOARD_ACTION_TYPES.REMOVE_BOARD,
    payload
  });

};