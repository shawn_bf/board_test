import BoardElement from "../../domains/board-element.domain";
import Board from "../../domains/board.domain";
import { GENDER } from "../../enums/gender.enum";

const EMILIA_CLARKE = new BoardElement({
  name: 'Emilia Clarke',
  image: 'https://media.glamour.com/photos/5a142d52a778886079dd984a/master/w_644,c_limit/EMILIA.jpg',
  score: 5,
  location: 'London',
  birthDay: '23/10/1986',
  gender: GENDER.FEMALE
});

const SOPHIE_TURNER = new BoardElement({
  name: 'Sophie Turner',
  image: 'https://i.ytimg.com/vi/qSB6pa78t-0/maxresdefault.jpg',
  score: 4,
  location: 'Northampton',
  birthDay: '21/02/1996',
  gender: GENDER.FEMALE
});

const KIT_HARINGTON = new BoardElement({
  name: 'Kit Harington',
  image: 'https://www.unitedagents.co.uk/sites/default/files/styles/custom_crop/public/thumbnails/image/ms12805_1.jpg?itok=SsrIVc2d',
  score: 3.2,
  location: 'Acton, London',
  birthDay: '26/12/1986',
  gender: GENDER.MALE
});

const PETER_DINKLAGE = new BoardElement({
  name: 'Peter Dinklage',
  image: 'https://pbs.twimg.com/media/DKleUMUVYAATGP2.jpg',
  score: 5,
  location: 'Morristown, New Jersey',
  birthDay: '11/06/1969',
  gender: GENDER.MALE
});


const MockedData = [
  new Board({ name: 'Wish to interview', elements: [ EMILIA_CLARKE, SOPHIE_TURNER ] }),
  new Board({ name: 'Planning to interview', elements: [ PETER_DINKLAGE ] }),
  new Board({ name: 'Interviewed', elements: [ KIT_HARINGTON ] })
];

export default MockedData;