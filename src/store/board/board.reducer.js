import BOARD_ACTION_TYPES from "./board.action-types";
import BoardMockedData from './board.mock';
import { DROP_LIST } from "../../enums/droplist.enum";
import Board from "../../domains/board.domain";

const initialState = BoardMockedData;

const BoardReducer = ( state = initialState, action ) => {

  switch ( action.type ) {

    case BOARD_ACTION_TYPES.ADD_NEW_BOARD: {
      return [ ...state, new Board({ name: action.payload }) ];
    }

    case BOARD_ACTION_TYPES.REMOVE_BOARD: {
      const newState = Object.assign([], state);
      const findIdxBoard = newState.findIndex(board => board.id === action.payload.id);

      newState.splice(findIdxBoard, 1);

      return newState;
    }

    case BOARD_ACTION_TYPES.MOVE_ITEM_TO_LIST: {

      const { dragStarted, dropped } = action.payload;

      if ( !dragStarted || !dropped ) {
        return state;
      }

      let newState = Object.assign([], state);

      switch ( dragStarted.dropList ) {

        case DROP_LIST.BOARD: {
          newState = moveBoardItems(newState, dragStarted, dropped);
          break;
        }

        case DROP_LIST.BOARD_OVERVIEW: {
          newState = moveBoardOverviewItems(newState, dragStarted, dropped);
          break;
        }

        default: {
        }

      }

      return newState;
    }

    default: {
      return state;
    }

  }

};

const moveBoardItems = ( newState, dragStarted, dropped ) => {
  let indexOfBoard = -1;
  let element = null;
  let elementIndex = -1;

  // first: find the element which was dragged
  newState.forEach(( board, index ) => {
    const boardElementIndex = board.elements.findIndex(boardElement => boardElement.id === dragStarted.itemId);
    if ( boardElementIndex !== -1 ) {
      indexOfBoard = index;
      element = board.elements[ boardElementIndex ];
      elementIndex = boardElementIndex;
    }

  });

  if ( indexOfBoard === -1 || element === null ) {
    return newState;
  }

  // second: remove the element from the current list
  newState[ indexOfBoard ].elements.splice(elementIndex, 1);

  // third: place it in the dropped list
  const indexOfNewList = newState.findIndex(board => board.id === dropped.itemId);

  if ( indexOfNewList !== -1 ) {
    newState[ indexOfNewList ].elements.splice(dropped.index, 0, element);
  }

  return newState;
};

const moveBoardOverviewItems = ( newState, dragStarted, dropped ) => {

  const findIndexBoardStarted = newState.findIndex(board => board.id === dragStarted.itemId);
  const board = newState[ findIndexBoardStarted ];

  // remove the board that was dragged
  newState.splice(findIndexBoardStarted, 1);
  // add the dragged board on the index it was dropped
  newState.splice(dropped.index, 0, board);

  return newState;
};

export default BoardReducer;