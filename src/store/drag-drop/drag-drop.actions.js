import DRAG_DROP_ACRION_TYPES from "./drag-drop.action-types";

export const DRAG_STARTED = payload => dispatch => {
  dispatch({
    type: DRAG_DROP_ACRION_TYPES.DRAG_STARTED,
    payload
  });
};

export const DRAG_ENDED = () => dispatch => {
  dispatch({
    type: DRAG_DROP_ACRION_TYPES.DRAG_ENDED
  });
};


export const DROPPED = payload => dispatch => {

  dispatch({
    type: DRAG_DROP_ACRION_TYPES.DROPPED,
    payload
  });

};