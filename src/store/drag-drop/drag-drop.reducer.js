import DRAG_DROP_ACRION_TYPES from "./drag-drop.action-types";

const initialState = null;

const DragDropReducer = ( state = initialState, action ) => {

  switch ( action.type ) {

    case DRAG_DROP_ACRION_TYPES.DRAG_STARTED: {

      const output = { dragStarted: action.payload };

      if ( state && state.dragStarted ) {
        return state;
      }

      return Object.assign({ ...state }, output);
    }

    case DRAG_DROP_ACRION_TYPES.DRAG_ENDED: {
      return null;
    }

    case DRAG_DROP_ACRION_TYPES.DROPPED: {

      const output = { dropped: action.payload };

      if ( state && state.dropped && state.dropped.dropList === state.dragStarted.dropList ) {
        return state;
      }

      return Object.assign({ ...state }, output);
    }

    default: {
      return state;
    }

  }

};

export default DragDropReducer;