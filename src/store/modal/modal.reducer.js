import { MODAL_ACTION_TYPES } from "./modal.action-types";

const initialState = null;

const ModalReducer = ( state = initialState, action ) => {

  switch ( action.type ) {

    case MODAL_ACTION_TYPES.OPEN_MODAL: {
      return action.payload;
    }

    case MODAL_ACTION_TYPES.CLOSE_MODAL: {
      return null;
    }

    default: {
      return state;
    }

  }

};

export default ModalReducer;