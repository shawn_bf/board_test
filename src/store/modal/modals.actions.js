import { MODAL_ACTION_TYPES } from "./modal.action-types";

export const OPEN_MODAL = payload => dispatch => {
  dispatch({
    type: MODAL_ACTION_TYPES.OPEN_MODAL,
    payload
  })
};

export const CLOSE_MODAL = () => dispatch => {
  dispatch({
    type: MODAL_ACTION_TYPES.CLOSE_MODAL
  })
};
