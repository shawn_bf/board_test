import { combineReducers } from "redux";

import BoardReducer from './board/board.reducer';
import TranslateReducer from "./translate/translate.reducer";
import DragDropReducer from "./drag-drop/drag-drop.reducer";
import ModalReducer from "./modal/modal.reducer";

const rootReducer = combineReducers({
  boards: BoardReducer,
  translate: TranslateReducer,
  dragDrop: DragDropReducer,
  modal: ModalReducer
});

export default rootReducer;