import TRANSLATE_ACTION_TYPES from "./translate.action-types";
import translations from '../../translations/index';

const initialState = translations.default;

const TranslateReducer = ( state = initialState, action ) => {

  switch ( action.type ) {

    case (TRANSLATE_ACTION_TYPES.CHANGE_LANGUAGE): {
      return action.payload
    }

    default: {
      return state;
    }

  }

};

export default TranslateReducer;