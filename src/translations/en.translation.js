export const EN = {

  GENDERS: {
    MALE: 'Male',
    FEMALE: 'Female'
  },

  MODALS: {

    ADD_NEW_BOARD: {
      TITLE: 'Add new board',
      LABEL: 'Board name',
      INPUT_PLACEHOLDER: '-- board name --',
      BUTTONS: {
        ADD: 'Add',
        CANCEL: 'Cancel'
      }
    }

  },

  BOARD: {

    TOOLTIP: {

      REMOVE_BOARD: 'Remove'

    }

  }

};