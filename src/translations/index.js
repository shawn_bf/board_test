import { EN } from "./en.translation";

const Languages = {

  EN: EN,

  default: EN
};

export default Languages;